<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DepartmentRequest;
use App\Repository\Master\DepartmentRepository;

class DepartmentController extends Controller
{
    protected $departmentRepository;

    public function __construct(DepartmentRepository $departmentRepository){
        $this->departmentRepository = $departmentRepository;
    }

    public function index(){
        $departments = $this->departmentRepository->index();

        return view('master.department.index')->with([
            'departments' => $departments
        ]);
    }

    public function create(){
        return view('master.department.create');
    }

    public function store(DepartmentRequest $request){
        $department = $this->departmentRepository->store($request);

        if($department){
            return redirect()->route('department.index')->with('success', 'Department Created Successfully');
        }
    }

    public function edit($id){
        $department = $this->departmentRepository->store($id);

        return view('master.department.update')->with([
            'department' => $department
        ]);
    }

    public function update(DepartmentRequest $request){
        $department = $this->departmentRepository->update($request);

        if($department){
            return redirect()->route('department.index')->with('success', 'Department Updated Successfully');
        }
    }

    public function destroy(Request $request){
        $department = $this->departmentRepository->destroy($request);

        if($department){
            return redirect()->route('department.index')->with('success', 'Department Removed Successfully');
        }
    }
}
