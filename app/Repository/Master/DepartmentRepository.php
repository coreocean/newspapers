<?php

namespace App\Repository\Master;

use App\Models\Department;
use Illuminate\Support\Facades\Auth;

class DepartmentRepository{
    public function index(){
        return Department::select('id', 'name', 'initial')->get();
    }

    public function store($request){
        $department = new Department;
        $department->name = $request->name;
        $department->initial = $request->initial;
        $department->created_by = Auth::user()->id;

        if($department->save()){
            return true;
        }
    }

    public function edit($id){
        return Department::find($id);
    }

    public function update($request){
        $department = Department::find($request->id);
        $department->name = $request->name;
        $department->initial = $request->initial;
        $department->updated_by = Auth::user()->id;

        if($department->save()){
            return true;
        }
    }



    public function destroy($request){
        $department = Department::find($request->id);
        $department->deleted_at = time();
        $department->deleted_by = Auth::user()->id;

        if($department->save()){
            return true;
        }
    }
}
