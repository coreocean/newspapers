<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AdvertiseCostController;
use App\Http\Controllers\BannerSizeController;
use App\Http\Controllers\BudgetProvisionController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\FinancialYearController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\NewsPaperController;
use App\Http\Controllers\NewsPaperTypeController;
use App\Http\Controllers\PrintTypeController;
use App\Http\Controllers\PublicationTypeController;
use App\Http\Controllers\SignatureController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [LoginController::class, 'login'])->name('admin.login');

Route::group(['middleware' => ['auth']], function() {

    Route::get('dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');

    Route::resource('advertise-cost', AdvertiseCostController::class);

    Route::resource('banner-size', BannerSizeController::class);

    Route::resource('budget-provision', BudgetProvisionController::class);

    Route::resource('department', DepartmentController::class);

    Route::resource('financial-year', FinancialYearController::class);

    Route::resource('language', LanguageController::class);

    Route::resource('news-paper', NewsPaperController::class);

    Route::resource('news-paper-type', NewsPaperTypeController::class);

    Route::resource('print-type', PrintTypeController::class);

    Route::resource('publication-type', PublicationTypeController::class);

    Route::resource('signature', SignatureController::class);
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
